import os
import re
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string

N_CPU = 2


def requirements():
    return({'ncpu': N_CPU, 'time': '800:00:00', 'mem': '6gb'})


def results(argv):
    sv_type = argv['--sv_type']
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    output = re.sub('.vcf$', '', output)
    output = '%s_%s' % (re.sub('.bcf$', '', output), sv_type)
    return({'vcf': '%s.vcf.gz' % output, 'idx': '%s.vcf.gz.csi' % output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Delly legacy version. Structural '
                                       'variant discovery by integrated '
                                       'paired-end  and split-read analysis'),
                                 add_help=False)


def delly_legacy_args(parser, subparsers, argv):
    parser.add_argument('-n', '--normal', dest='normal',
                        help='Normal sorted and indexed BAM file',
                        required=True)
    parser.add_argument('-t', '--tumor', dest='tumor',
                        help='Tumor sorted and indexed BAM file',
                        required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output BCF file', required=True)
    parser.add_argument('--sv_type', dest='sv_type', action='store',
                        choices=['DEL', 'DUP', 'INV', 'TRA', 'INS'],
                        help='SV type, default DEL', default='DEL')
    parser.add_argument('--no-exclude', dest='no_exclude',
                        help='Do not exclude regions', action='store_true')
    return parser.parse_args(argv)


def delly_legacy(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = delly_legacy_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['tabix']))
    module('add', program_string(profile.programs['delly_legacy']))

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)

    if args.no_exclude is False:
        ref_exclude = profile.files['gencode_excl']
        log.log.info('Use exclude regions file %s' % ref_exclude)

    log.log.info('Set environment variable OMP_NUM_THREADS=%s' % N_CPU)
    os.environ['OMP_NUM_THREADS'] = '%s' % N_CPU

    output = re.sub('.gz$', '', args.out)
    output = re.sub('.vcf$', '', args.out)
    output = '%s_%s' % (re.sub('.bcf$', '', output), args.sv_type)

    output_dir, output_file = os.path.split(os.path.abspath(output))
    if not os.path.exists(output_dir):
        log.log.info('Create output directory %s' % output_dir)
        os.makedirs(output_dir)

    log.log.info('Preparing delly command line')
    delly_cmd = ['delly', '-t', args.sv_type, '-g', genome,
                 '-s', '9', '-q', '1', '-o', '%s.vcf' % output]
    if args.no_exclude is False:
        delly_cmd += ['-x', ref_exclude]
    delly_cmd += [args.tumor, args.normal]

    log.log.info('Preparing bgzip and tabix command lines')
    bgzip_cmd = ['bgzip', '%s.vcf' % output]
    tabix_cmd = ['tabix', '-p', 'vcf', '-C', '%s.vcf.gz' % output]

    delly_cmd = shlex.split(' '.join(map(str, delly_cmd)))
    bgzip_cmd = shlex.split(' '.join(map(str, bgzip_cmd)))
    tabix_cmd = shlex.split(' '.join(map(str, tabix_cmd)))

    log.log.info(' '.join(map(str, delly_cmd)))
    log.log.info('Execute delly with python subprocess.Popen')
    delly_proc = subprocess.Popen(delly_cmd)
    out0 = delly_proc.communicate()[0]

    log.log.info(' '.join(map(str, bgzip_cmd)))
    log.log.info('Execute bgzip with python subprocess.Popen')
    bgzip_proc = subprocess.Popen(bgzip_cmd)
    out1 = bgzip_proc.communicate()[0]

    log.log.info(' '.join(map(str, tabix_cmd)))
    log.log.info('Execute tabix_cmd with python subprocess.Popen')
    tabix_proc = subprocess.Popen(tabix_cmd)
    out2 = tabix_proc.communicate()[0]

    log.log.info('Terminate delly')
